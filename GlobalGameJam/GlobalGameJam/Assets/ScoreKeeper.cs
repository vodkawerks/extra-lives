﻿using UnityEngine;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

	public static int currentScore = 0;

		public static void StackedBlock() {
		currentScore += 10;
	}

		public static void BrickBroken() {
		currentScore += 20;
	}

		public static void AlienKilled() {
		currentScore += 10;
	}

		public static void SecondSurvived() {
		currentScore += 1;
	}

		public static void Choice1() {
		currentScore += 20;
	}

		public static void Choice2() {
		currentScore += 40;
	}

		public static void OneDollarTip() {
		currentScore += 150;
	}

		public static void TwoDollarTip() {
				currentScore += 200;
		}

		public static void PizzaShop() {
				currentScore += 100;
		}
}
