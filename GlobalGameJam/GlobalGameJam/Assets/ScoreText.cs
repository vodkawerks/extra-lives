﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreText : MonoBehaviour {

	int lastScore;
	Text scoreText;
	[SerializeField]
	ParticleSystem effect;

	// Use this for initialization
	void Start () {
		lastScore = ScoreKeeper.currentScore;
		scoreText = gameObject.GetComponent<Text>();
	}

	// Update is called once per frame
	void Update () {
		if (lastScore != ScoreKeeper.currentScore) {
			OnScoreChanged(ScoreKeeper.currentScore);
		}
	}

	void OnScoreChanged(int newScore) {
				if (effect == null)
						return;

		lastScore = newScore;
		float digits = Mathf.Floor(Mathf.Log10(newScore)) + 1;
		float width = 30 * digits;
		float offset = width / 2f;
		effect.transform.localScale = new Vector3(digits, 1, 1);
		effect.transform.localPosition = new Vector3(175f + offset, -20.7f, 0);
		effect.Play ();
		scoreText.text = "SCORE: " + lastScore;
	}
}
