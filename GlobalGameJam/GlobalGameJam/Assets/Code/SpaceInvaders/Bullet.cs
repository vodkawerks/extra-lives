﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	private SpriteRenderer renderer;

	public Vector3 direction = Vector3.up;
	public enum BulletAffinity {KILL_ENEMY, KILL_PLAYER};
	public BulletAffinity affinity = BulletAffinity.KILL_ENEMY;

	// Use this for initialization
	void Start () {
		renderer = gameObject.renderer as SpriteRenderer;
		rigidbody2D.velocity = direction * SpaceInvaders.Instance.bulletSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		if (!renderer.isVisible) {
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (affinity == BulletAffinity.KILL_ENEMY && other.tag == "Enemy") {
			Enemy enemy = other.gameObject.GetComponent<Enemy>();
			enemy.OnDamage();
						ScoreKeeper.AlienKilled ();
		} else if (affinity == BulletAffinity.KILL_PLAYER && other.tag == "Player") {
			SpaceInvaders.Instance.OnDamagePlayer();
		}
	}
}
