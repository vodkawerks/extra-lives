﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpaceInvaders : MonoBehaviour {

	public static SpaceInvaders Instance;

	public ScrollController scrollController;
	public Transform Player;
	public GameObject bulletPrefab;
	public GameObject enemyPrefab;
		public Transform TextAdventure;
		public Text FinalScore;
	public float bulletInterval;
	public float bulletSpeed;
	public float enemyRowInterval;
	public float enemySpeed;
	public ParticleSystem explode;
		public Vector3 EndingCameraPosition = new Vector3(29.07f, 78.72f, -2.17f);

	private float timeLastShot;
	private float timeLastEnemy;
	private float playerHealth = 1;
		private bool _gameIsRunning;
		private bool _cameraIsSet;

	// Use this for initialization
	void Start () {
		if (Instance == null) {
			Instance = this;
		} else {
			Destroy(this);
		}
		scrollController.Object = Player;
		scrollController.YAxisEnabled = true;
		timeLastShot = -bulletInterval;
		timeLastEnemy = -enemyRowInterval;
		Player.rigidbody2D.velocity = Vector3.zero;
				_gameIsRunning = true;
				_cameraIsSet = false;
	}
	
	// Update is called once per frame
	void Update () {
				if (!_gameIsRunning)
						return;

				if (!_cameraIsSet) {
						if (Vector3.Distance (Player.position, EndingCameraPosition) < 2f) {
								_cameraIsSet = true;
						} else {
								Camera.main.GetComponent<CameraController> ().UseSpring = false;
								Camera.main.transform.position = Vector3.Slerp (Camera.main.transform.position, EndingCameraPosition, 1f * Time.deltaTime);
						}
				}

		if (Time.time > timeLastShot + bulletInterval) {
			Shoot();
		}
		if (Time.time > timeLastEnemy + enemyRowInterval) {
			SpawnEnemyRow();
		}
	}

	void Shoot() {
				AudioPlayer.Instance.PlayerLazer ();
		Instantiate(bulletPrefab, Player.position + Vector3.up * .6f, Quaternion.identity);
		timeLastShot = Time.time;
	}

	void SpawnEnemyRow() {
		for (int x = -5; x < 5; x++) {
						GameObject o = Instantiate (enemyPrefab, new Vector3 (transform.position.x + x, transform.position.y + 5, -10f), Quaternion.identity) as GameObject;
						o.transform.localPosition = new Vector3 (o.transform.localPosition.x, o.transform.localPosition.y, 10f);
		}
		timeLastEnemy = Time.time;
	}

	void Fail() {
		Debug.Log("Failed.");
		explode.transform.position = Player.position;
		Player.gameObject.SetActive(false);
		explode.Play ();
	//	gameObject.SetActive(false);

				TextAdventure.gameObject.SetActive (true);
				TextAdventure.transform.Find ("Greeting").gameObject.SetActive (false);
				TextAdventure.transform.Find ("Score_screen").gameObject.SetActive (true);

				scrollController.transform.parent.gameObject.SetActive (false);
				FinalScore.text = "Score: " + ScoreKeeper.currentScore;
	}

	void Win() {
		Debug.Log("Win!");
				_gameIsRunning = false;
	}

	public void OnDamagePlayer() {
		Debug.Log("Player Hit!");
		playerHealth -= 1;
		if (playerHealth <= 0) {
			Fail ();
						_gameIsRunning = false;
		}
	}
}
