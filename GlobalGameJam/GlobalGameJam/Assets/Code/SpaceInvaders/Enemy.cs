﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	private float lastShot;
	public GameObject bulletPrefab;
	private float shotInterval;
	private float health = 1;
	private float xOffset;
	private float originalX;
	// Use this for initialization
	void Start () {
		shotInterval = UnityEngine.Random.Range(1f, 3.0f);
		lastShot = Time.time;
		originalX = transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
		xOffset += Random.Range(-.5f, .5f) * Time.deltaTime;
		xOffset = Mathf.Clamp(xOffset, -3f, 3f);
		transform.position = new Vector3(
			originalX + xOffset,
			transform.position.y - SpaceInvaders.Instance.enemySpeed * Time.deltaTime,
			0);

				transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, 10f);
		if (!renderer.isVisible) {
			Destroy (gameObject);
		}
		if (Time.time > lastShot + shotInterval) {
			Shoot();
		}
	}

	void Shoot() {
		GameObject obj = Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity) as GameObject;
		obj.transform.localPosition = new Vector3 (obj.transform.localPosition.x, obj.transform.localPosition.y, 10f);
		Bullet bullet = obj.GetComponent<Bullet>();
		bullet.affinity = Bullet.BulletAffinity.KILL_PLAYER;
		bullet.direction = Vector3.down;
		lastShot = Time.time;

	}

	public void OnDamage() {
		health -= 1;
		if (health <= 0) {
			Destroy(gameObject);
		}
	}
}
