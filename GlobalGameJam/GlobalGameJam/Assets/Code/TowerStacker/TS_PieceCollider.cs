﻿using UnityEngine;
using System.Collections;

public class TS_PieceCollider : MonoBehaviour {
		#region Private Variables
		private bool _hasHitCollider;
		#endregion

		#region Public Methods
		public void OnCollisionEnter2D(Collision2D collision)
		{
				if (collision.gameObject.tag == "SS_FloorShard") {
						SideScroller.Instance.StartGame ();
				}
				else if (collision.gameObject.name == "Floor")
						TowerStacker.Instance.Fail ();
				else if (!_hasHitCollider) {
						if (collision.gameObject.transform.position.y >= 10f)
								TowerStacker.Instance.Win ();
						else
								TowerStacker.Instance.SpawnNewPiece ();
				} 

				_hasHitCollider = true;
		}
		#endregion
}
