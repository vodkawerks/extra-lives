﻿using UnityEngine;
using System;
using System.Collections;

public class TowerStacker : MonoBehaviour {

		#region Public Static Properties
		public static TowerStacker Instance;
		#endregion

		#region Public Properties
		public Transform SpawnPoint;
		public Transform Prefab;
		public ScrollController ScrollController;
		public Transform Tower;
		public BreakOut Breakout;
		public Transform Player;
		#endregion

		#region Private Variables
		private DateTime _lastCreatedTime;
		private bool _gameIsRunning;
		private int _failCount = 0;
		private int _stackBlockedCount;
		#endregion
		// Use this for initialization
		void Start () {
					if (Instance == null)
							Instance = this;

				_lastCreatedTime = DateTime.Now;
				Player.rigidbody2D.isKinematic = true;
				_gameIsRunning = false;

				//StartGame ();
		}
		
		// Update is called once per frame
		void Update () {
		}

		#region Public Static Methods
		public void StartGame()
		{
				Physics2D.gravity = new Vector2 (0f, -6f);
				SpawnNewPiece ();
				Player.rigidbody2D.isKinematic = false;
				CanvasGroup x = ScrollController.transform.parent.gameObject.GetComponent<CanvasGroup> ();
				x.interactable = true;
				x.blocksRaycasts = true;
				x.alpha = 1f;
				_gameIsRunning = true;

				foreach (Transform t in transform) {
						if (t.GetComponent<FUKinematic> () != null)
								continue;

						Rigidbody2D[] rigidbodies = t.GetComponentsInChildren<Rigidbody2D> ();

						foreach (var rb in rigidbodies)
								rb.isKinematic = false;
				}

		}

		public void SpawnNewPiece()
		{
				if (_lastCreatedTime.AddMilliseconds(100) > DateTime.Now || !_gameIsRunning)
						return;

				if (_stackBlockedCount == 1)
						BubbleMessage.ShowMessage("ts");

				_stackBlockedCount++;
				AudioPlayer.Instance.TowerPlacement ();
				ScoreKeeper.StackedBlock ();
				_lastCreatedTime = DateTime.Now;

				GameObject obj = Instantiate (Resources.Load(Prefab.name), SpawnPoint.position, Quaternion.identity) as GameObject;

				SpawnPoint.position = new Vector3 (SpawnPoint.position.x  + UnityEngine.Random.Range (-.5f, .5f), SpawnPoint.position.y, SpawnPoint.position.z);

				obj.name = "TS_BLOCK";

				obj.transform.parent = Tower;

				obj.transform.localScale = new Vector3 (obj.transform.localScale.x *UnityEngine.Random.Range (.4f,1f), obj.transform.localScale.y, obj.transform.localScale.z);

				ScrollController.Object = obj.transform;
		}

		public void Fail()
		{
				if (_failCount == 0) {
						_failCount++;
						Camera.main.GetComponent<CameraController> ().UseSpring = false;
						SpawnNewPiece ();

						return;
				}
				_gameIsRunning = false;
				Debug.Log ("Failed.");
				AudioPlayer.Instance.FailThenRunner ();
//				foreach (Transform t in transform) {
//						Rigidbody2D[] rigidbodies = t.GetComponentsInChildren<Rigidbody2D> ();
//
//						foreach (var rb in rigidbodies) {
//								rb.isKinematic = true;
//						}
//				}

				Physics2D.gravity = new Vector2 (0f, -9.8f);

				GameObject floor = GameObject.FindGameObjectWithTag ("TS_Floor");

				floor.renderer.enabled = false;
				floor.collider2D.enabled = false;

				GameObject[] floorShards = GameObject.FindGameObjectsWithTag ("TS_FloorShard");

				foreach (GameObject fs in floorShards) {
						fs.renderer.enabled = true;
						fs.collider2D.enabled = true;

						fs.rigidbody2D.isKinematic = false;
						fs.rigidbody2D.MoveRotation (UnityEngine.Random.Range (1, 90));
				}

				Camera.main.GetComponent<CameraController> ().UseSpring = true;
		}
	
		public void Win()
		{
				_gameIsRunning = false;
				Debug.Log ("Win");
				AudioPlayer.Instance.WinThenBreakout ();
				GameObject.Find ("Player").transform.parent = Tower;

				foreach (Transform t in Tower)
						t.rigidbody2D.isKinematic = true;


				Camera.main.GetComponent<CameraController> ().ConnectedObject = Tower;
				Camera.main.GetComponent<CameraController> ().UseSpring = true;
				Breakout.enabled = true;
		}

		public void ButtonPress()
		{
		}
		#endregion
}
