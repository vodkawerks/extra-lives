﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AudioPlayer : MonoBehaviour {

	private string currentMusic;
		public static  AudioPlayer Instance;

	// Use this for initialization
	void Start () {
		if (Instance == null) {
			Instance = this;
		} else {
			Destroy(this);
		}

	}

	// Update is called once per frame
	void Update () {

	}

	public void NewGame() {
		Play("New Game");
	}

	public void GameOver() {
		Play("Game Over");
	}

	public void Jump() {
		Play ("Jump");
	}

	public void Bounce() {
		Play ("Ball Bounce");
	}

	public void AlienLazer() {
				Play ("Alien Lazer", .5f, false, true);
	}
	
	public void PlayerLazer() {
				Play ("Player Lazer", 1f, true, false);
	}
	
		public void BrickBreak() {
				Play ("Brick Break");
		}	
	
		public void TowerPlacement() {
				Play ("Tower Placement");
		}


		public void SpaceInvaders() {
				Play ("Space Invaders", .5f, false, true);
		}


	public void Stacker() {
		Stop (currentMusic);
		currentMusic = "Stacker";
				//Play(currentMusic, .3f, false, true);
	}

	public void WinThenBreakout() {
		StartCoroutine(PlaySequence("Winning", 2.5f, "Breakout"));
	}

	public void WinThenSpaceInvaders() {
		StartCoroutine(PlaySequence("Winning", 2.5f, "Space Invaders"));
	}

	public void FailThenRunner() {
		StartCoroutine(PlaySequence("Game Over", 4f, "Runner"));
	}

	public void FailThenText() {
		StartCoroutine(PlaySequence("Game Over", 4f, "Text Adventure"));
	}

		public void Play(string name)
		{
				currentMusic = name;
				AudioManager.Play (name);
		}

		public void Play(string name, float volume, bool ignore=true, bool loop=false)
		{
				currentMusic = name;
				AudioManager.Play (name, ignore, loop, volume);
		}

	public void Stop(string name) {
		try {
			AudioManager.Stop(name);
		} catch (NullReferenceException) {
			Debug.Log("Couldn't find " + currentMusic + " to stop");
		}
	}

	private IEnumerator PlaySequence(string sound1, float timeout, string sound2) {
				AudioManager.StopAll ();
		Stop (currentMusic);
		currentMusic = sound1;
		Play (currentMusic, .5f, true, false);
		yield return new WaitForSeconds(timeout);
		Stop (currentMusic);
		currentMusic = sound2;
				Play (currentMusic, .5f, false, true);
	}
}
