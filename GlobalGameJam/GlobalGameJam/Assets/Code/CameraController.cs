﻿using System;
using UnityEngine;

public class CameraController : MonoBehaviour
{
		#region Public Properties
		public Transform ConnectedObject;
		public bool UseSpring;
		public float Speed = 1f;
		#endregion

		#region Public Properties
		public void Update()
		{
				if (!UseSpring)
						return;

				transform.position = Vector3.Slerp (transform.position, new Vector3 (ConnectedObject.position.x, ConnectedObject.position.y, transform.position.z), Speed * Time.deltaTime);
		}
		#endregion
}

