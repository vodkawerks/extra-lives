﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScrollController : UIBehaviour
{
		#region Public Properties
		public Transform Object;
		public bool YAxisEnabled = false;
		public float speed = 0.8f;
		public Button Button {
				get {
						return transform.Find ("Button").GetComponent<Button> ();
				}
		}
		#endregion

		#region Public Methods
		public void Drag(BaseEventData data)
		{
				PointerEventData pData = data as PointerEventData;

				if (pData == null || Object == null)
						return;

				Vector3 newPos;
				if (YAxisEnabled) {
					newPos = new Vector3 (Object.position.x + (pData.delta.x * speed * Time.deltaTime), 
			                              Object.position.y + (pData.delta.y * speed * Time.deltaTime), 
			                              Object.position.z);
				} else {
					newPos = new Vector3 (Object.position.x + (pData.delta.x * speed * Time.deltaTime), 
			                              Object.position.y, 
			                              Object.position.z);
				}

				Object.position = newPos;
		}
		#endregion
}

