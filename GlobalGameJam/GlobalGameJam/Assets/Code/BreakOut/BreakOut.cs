﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BreakOut : MonoBehaviour {

		#region Public Static Properties
		public static BreakOut Instance;
		#endregion

		#region Public Properties
		public Transform player;
		public Transform SpawnPoint;
		public Transform PaddleMovePoint;
		public Transform BallPrefab;
		public Transform BrickPrefab;
		public Transform PaddleObject;
		public BreakoutFloor Floor;
		public PhysicsMaterial2D PhysicMaterial;
		public ScrollController ScrollController;
		public GameObject spaceInvaders;
		public Transform ExitTransform;
		public Transform StartTransform;
		public Quaternion PaddleRotation = new Quaternion(0f, 0f, .7f, .7f);
		public Vector3 PaddleScale = new Vector3(.75f, .75f, .75f);
		public Vector3 EndingCameraPosition  = new Vector3(28.9f, 19.52f, .24f);
		public Transform FailEndingPosition;
		public Transform TextAdventure;
		public float SlerpTime = .7f;
		public int brickColumns;
		public int brickRows;
		public float difficulty = 1.0f;
		#endregion

		#region Private Variables
		private int bricksLeft;
		private bool _isPositioningPlayer;
		private bool _isPositioningBall;
		private bool _isPositioningForSI;
		private bool _goForSI;
		private bool _isPositioningForTA;
		private int _failCount;
		#endregion

		void Start () {
				if (Instance == null) {
						Instance = this;
				} else {
						Destroy(this);
				}
				ScrollController.Object = PaddleObject;
				//SpawnBall();
				_isPositioningPlayer = true;
				Floor.gameObject.SetActive (false);
		}

		// Update is called once per frame
		void Update () {

				if (_isPositioningPlayer) {
						if (Vector3.Distance (PaddleObject.position, PaddleMovePoint.transform.position) > 10.5f) {
								PaddleObject.position = Vector3.Slerp (PaddleObject.position, PaddleMovePoint.transform.position, SlerpTime * Time.deltaTime);
								PaddleObject.rotation = Quaternion.Slerp (PaddleObject.rotation, PaddleRotation, SlerpTime * Time.deltaTime);
								PaddleObject.localScale = Vector3.Slerp (PaddleObject.localScale, PaddleScale, SlerpTime * Time.deltaTime);
						} else {
								PaddleObject.rotation = PaddleRotation;
								PaddleObject.localPosition = new Vector3 (PaddleObject.localPosition.x, PaddleObject.localPosition.y, 0f);
								Floor.gameObject.SetActive (true);
								SpawnBricks ();
								_isPositioningBall = true;
								_isPositioningPlayer = false;
								Camera.main.GetComponent<CameraController> ().UseSpring = false;
								player.parent = null;
								player.rotation = Quaternion.identity;
						}
				} else if (_isPositioningBall) {
						if (Vector3.Distance (player.position, SpawnPoint.position) > 1f) {
								Camera.main.transform.position = Vector3.Slerp (Camera.main.transform.position, EndingCameraPosition, SlerpTime * Time.deltaTime);
								player.position = Vector3.Slerp (player.position, SpawnPoint.position, SlerpTime * Time.deltaTime);
						} else {
								_isPositioningBall = false;
								player.localPosition = new Vector3 (player.localPosition.x, player.localPosition.y, 10f);
								player.rigidbody2D.isKinematic = false;
								player.rigidbody2D.gravityScale = 0;
								player.gameObject.rigidbody2D.velocity = new Vector3 (-1, 1, 0).normalized * 3;
								player.gameObject.rigidbody2D.fixedAngle = true;

								foreach (Collider2D x in PaddleObject.GetComponentsInChildren<Collider2D>()) {
										x.sharedMaterial = PhysicMaterial;
								}
						}
				} else if (_isPositioningForSI) {
						player.rotation = Quaternion.identity;

						if (Vector3.Distance (player.position, StartTransform.position) > 3f) {
								Debug.Log ("1.1");
								player.position = Vector3.Slerp (player.position, StartTransform.position, SlerpTime * Time.deltaTime);
						} else {
								player.GetComponent<PlayerRocketsZOOM> ().Effects.Play ();
								BubbleMessage.ShowMessage ("si");
								_isPositioningForSI = false;
								Debug.Log ("1.2");
								_isPositioningBall = false;
								player.rigidbody2D.velocity = Vector2.zero;
								_goForSI = true;

								transform.Find ("BO_Floor").gameObject.SetActive (false);
						}
							
				}
				else if (_goForSI) {

						if (Vector3.Distance (player.position, ExitTransform.position) > 1f)
								player.position = Vector3.Slerp (player.position, ExitTransform.position, SlerpTime * Time.deltaTime);
						else {
								spaceInvaders.GetComponent<SpaceInvaders> ().enabled = true;
								gameObject.GetComponent<BreakOut> ().enabled = false;
						}
				}
				else if (_isPositioningForTA) {

						if (Vector3.Distance (player.position, FailEndingPosition.position) > 10f)
								player.position = Vector3.Slerp (player.position, FailEndingPosition.position, SlerpTime * Time.deltaTime);
						else {
								player.rigidbody2D.isKinematic = true;
								player.rigidbody2D.gravityScale = 0f;
								ScrollController.gameObject.SetActive (false);

								TextAdventure.gameObject.SetActive (true);
								_isPositioningForTA = false;
						}
				}
		}

		#region Public Static Methods
		public void SpawnBall()
		{
				GameObject obj = Instantiate (
						Resources.Load(BallPrefab.name),
						SpawnPoint.position,
						Quaternion.identity) as GameObject;

				obj.name = "BO_Ball";

				obj.transform.parent = transform;
		}

		public void SpawnBricks()
		{
				float xScale = 1f;
				float yScale = .6f;
				float xOffset = -5f;
				float yOffset = 2f;
				for (int x=0; x<brickColumns; x++) {
						for (int y=0; y<brickRows; y++) {
								Vector3 brickPos = new Vector3(x, y, 0f);
								GameObject obj = Instantiate (
										Resources.Load(BrickPrefab.name),
										Vector3.zero,
										Quaternion.identity) as GameObject;

								obj.name = "BO_Brick";

								obj.transform.parent = transform;
								obj.transform.localPosition = new Vector3(x * xScale + xOffset, y * yScale + yOffset, 0f);
								bricksLeft += 1;
						}
				}
		}

		public void HitBrick() {
				ScoreKeeper.BrickBroken ();
				AudioPlayer.Instance.BrickBreak ();
				bricksLeft -= 1;
				if (bricksLeft <= 0) {
						Win();
				}
		}

		public void Fail()
		{
				if (_failCount == 0)
						_failCount = 1;
				else {
						Debug.Log ("Failed.");
						// Transition to text game
						player.rigidbody2D.velocity = Vector3.zero;
						player.rigidbody2D.isKinematic = true;
						player.rigidbody2D.gravityScale = 0f;
						_isPositioningForTA = true;
						Camera.main.GetComponent<CameraController> ().ConnectedObject = player;
						Camera.main.GetComponent<CameraController> ().UseSpring = true;
				}
		}

		public void Win()
		{
				Debug.Log ("Win");
				AudioPlayer.Instance.WinThenSpaceInvaders ();
				_isPositioningForSI = true;
				Camera.main.GetComponent<CameraController> ().ConnectedObject = player;
				Camera.main.GetComponent<CameraController> ().UseSpring = true;
				// Transition to Space Invaders
//				spaceInvaders.SetActive(true);
//				gameObject.SetActive(false);
		}
		#endregion

}
