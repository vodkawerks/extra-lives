﻿using UnityEngine;
using System.Collections;

public class BO_BallCollider : MonoBehaviour {
	#region Static Variables
	#endregion

	#region Private Variables
	[SerializeField]
	Vector2 initialDirection;

	float speed = 5;
		int collisionCOunt = 0;
	#endregion

	#region Public Methods
	public void Start() {
		//rigidbody2D.velocity = initialDirection.normalized * speed;
	}

	public void Update() {
		if (BreakOut.Instance == null)
						return;

		rigidbody2D.velocity *= 1 + (BreakOut.Instance.difficulty * Time.deltaTime);
	}

	public void OnCollisionEnter2D(Collision2D collision) {
		
				if (collision.gameObject.name == "BO_Floor" && collisionCOunt == 0) {
						collisionCOunt++;
						BubbleMessage.ShowMessage ("bo");
				}
		if (collision.gameObject.name == "BO_Brick") {
			Destroy(collision.gameObject);
			BreakOut.Instance.HitBrick();
		}
	}

	public void OnTriggerEnter2D(Collider2D other) {
		BreakOut.Instance.Fail();
	}

	#endregion
}
