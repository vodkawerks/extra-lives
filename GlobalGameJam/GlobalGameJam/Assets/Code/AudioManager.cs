﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class AudioManager
{
		#region Constants
		private static string AUDIO_CLIP_NAME_FORMAT = "Audio-{0}";
		#endregion

		#region Private Static Variables
		private static List<GameObject> _audioSources;
		private static bool _loopAudio;
		private static float _volume;
		private static float _minDistance;
		private static Vector3 _soundPosition;
		#endregion

		#region Public Static Methods
		public static void Stop(string source)
		{
				try {
						foreach (var aSource in _audioSources) {
								if (aSource.audio.clip.name.ToLower () == source.ToLower ())
										aSource.audio.Stop ();
						}
				} catch {
						_audioSources = new List<GameObject> ();
				}
		
		}

		public static void StopAll()
		{
				foreach (var aSource in _audioSources)
						aSource.audio.Stop();
		}

		public static void StopAll(string ignoreSource)
		{
				foreach (var aSource in _audioSources)
				{
						if (aSource.audio.clip.name.ToLower() != ignoreSource.ToLower())
								aSource.audio.Stop();
				}
		}

		public static bool IsPlaying(string source)
		{
				foreach (var aSource in _audioSources)
				{
						if (aSource.audio.clip.name.ToLower() == source.ToLower()) {
								return aSource.audio.isPlaying;
						}
				}
				return false;
		}

		public static void Play(string source)
		{
				Play(source, true, false, 1f, Vector3.zero, 1f);
		}

		public static void Play(string source, bool ignoreIsPlayingFlag)
		{
				Play(source, ignoreIsPlayingFlag, false, 1f, Vector3.zero, 1f);
		}

		public static void Play(string source, bool ignoreIsPlayingFlag, bool loopAudio)
		{
				Play(source, ignoreIsPlayingFlag, loopAudio, 1f, Vector3.zero, 1f);
		}

		public static void Play(string source, bool ignoreIsPlayingFlag, bool loopAudio, float volume)
		{
				Debug.Log (volume);
				Play(source, ignoreIsPlayingFlag, loopAudio, volume, Vector3.zero, 1f);
		}

		public static void Play(string source, bool ignoreIsPlayingFlag, bool loopAudio, float volume, Vector3 soundPosition)
		{
				Play(source, ignoreIsPlayingFlag, loopAudio, volume, soundPosition, 1f);
		}

		public static void Play(string source, bool ignoreIsPlayingFlag, bool loopAudio, float volume, Vector3 soundPosition, float minDistance)
		{
				try 
				{
					if (_audioSources == null)
							_audioSources = new List<GameObject>();

					if (!ignoreIsPlayingFlag)
					{
							foreach (var aSource in _audioSources)
							{
									if (aSource.audio.isPlaying && aSource.audio.clip.name.ToLower() == source.ToLower())
											return;
							}
					}
				}
				catch {
						_audioSources = new List<GameObject> ();
				}

				_loopAudio = loopAudio;
				_volume = volume;
				_soundPosition = soundPosition;
				_minDistance = minDistance;
				FindFreeAudioSource(source);
		}
		#endregion

		#region Private Static Methods
		private static void CreateNewAudioObject(string source, GameObject audioObject = null)
		{
				if (audioObject == null)
				{
						GameObject audioObj = new GameObject(string.Format(AUDIO_CLIP_NAME_FORMAT, _audioSources.Count));
						AudioSource audioSource = audioObj.AddComponent<AudioSource>();

						audioSource.minDistance = _minDistance;
						audioSource.clip = Resources.Load(source, typeof(AudioClip)) as AudioClip;
						audioSource.playOnAwake = true;
						audioSource.loop = _loopAudio;
						audioSource.volume = _volume;
						audioSource.transform.position = _soundPosition;
						audioSource.Play();
						_audioSources.Add(audioObj);
				}
				else
				{
						AudioSource audioSource = audioObject.GetComponent<AudioSource>();

						if (audioSource == null)
								audioSource = audioObject.AddComponent<AudioSource>();

						audioSource.minDistance = _minDistance;
						audioSource.clip = audioSource.clip.name.ToLower() == source.ToLower() ? audioSource.clip : Resources.Load(source, typeof(AudioClip)) as AudioClip;
						audioSource.playOnAwake = true;
						audioSource.loop = _loopAudio;
						audioSource.volume = _volume;
						audioSource.transform.position = _soundPosition;
						audioSource.Play();
				}
		}

		private static void FindFreeAudioSource(string source)
		{
				try 
				{
				if (_audioSources == null)
						return;

				GameObject freeAudioSource = _audioSources.FirstOrDefault(x => !x.audio.isPlaying && x.audio.clip.name.ToLower() == source.ToLower());

				if (freeAudioSource == null)
						freeAudioSource = _audioSources.FirstOrDefault(x => !x.audio.isPlaying);

				if (freeAudioSource == null)
						freeAudioSource = null;

				CreateNewAudioObject(source, freeAudioSource);
				}
				catch {
						_audioSources = new List<GameObject> ();
						CreateNewAudioObject(source, null);
				}
		}
		#endregion
}
