﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

		public void Start()
		{
				AudioPlayer.Instance.SpaceInvaders ();

		}

		public void Play()
		{
				AudioPlayer.Instance.Stacker ();
				gameObject.SetActive (false);
				TowerStacker.Instance.StartGame ();
		}

		public void Exit()
		{
				Application.Quit ();
		}
}
