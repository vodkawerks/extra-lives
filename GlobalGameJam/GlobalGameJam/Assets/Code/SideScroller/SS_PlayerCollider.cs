﻿using System;
using UnityEngine;

public class SS_PlayerCollider :MonoBehaviour
{
		#region Public Methods
		public void OnCollisionEnter2D(Collision2D collision)
		{
				if (collision.gameObject.tag == "SS_FloorShard") 
						SideScroller.Instance.ResetJump ();
				else if (collision.gameObject.tag == "SS_Floor")
						SideScroller.Instance.Fail ();
		}
		#endregion
}

