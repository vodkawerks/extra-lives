﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class SideScroller : MonoBehaviour {

		#region Public Static Properties
		public static SideScroller Instance;
		#endregion

		#region Public Properties
		public ScrollController ScrollController;
		public Transform GameBoard;
		public float Speed;
		public Transform Prefab;
		public float SpawnBlockIntervalDistance = 5f;
		public Transform LastPieceOnBoard;
		public Transform FailEndingPosition;
		public Transform TextAdventure;
		#endregion

		#region Private Variables
		private bool _gameIsRunning;
		private Transform _player;
		private float _lastSpawnedPosition;
		private float _jumpCount;
		private bool _fadeTSObjects;
		private DateTime _lastUpdatedScore;
		private bool _positionFail;
		#endregion
	// Use this for initialization

		#region Public Methods
		void Start () {
				if (Instance == null)
						Instance = this;
		}
	
		// Update is called once per frame
		void Update () {

				if (!_gameIsRunning && !_positionFail)
						return;

				if (_positionFail) {
						if (Vector3.Distance (_player.position, FailEndingPosition.position) > 10f)
								_player.position = Vector3.Slerp (_player.position, FailEndingPosition.position, .7f * Time.deltaTime);
						else {
								ScrollController.gameObject.SetActive (false);

								TextAdventure.gameObject.SetActive (true);
								_positionFail = false;
						}
				} else 
				{
					if (_lastUpdatedScore.AddMilliseconds (500f) < DateTime.Now) {
							_lastUpdatedScore = DateTime.Now;
							ScoreKeeper.SecondSurvived ();
					}

					if (_fadeTSObjects) {
							List<GameObject> tsObjects = GameObject.FindGameObjectsWithTag ("TS_FloorShard").ToList ();
							tsObjects.AddRange (GameObject.FindGameObjectsWithTag ("TS_BLOCK").ToList ());

							foreach (GameObject tsObj in tsObjects) {
									Color c = tsObj.renderer.material.color;

									c.a -= .01f;

									if (c.a <= 0f)
											c.a = 0f;

									tsObj.renderer.material.color = c;
							}

							if (tsObjects.All (x => x.renderer.material.color.a == 0)) {
									_fadeTSObjects = false;
									tsObjects.ForEach(x=> x.collider2D.enabled = false);
							}
					}


					GameBoard.position = new Vector3 (GameBoard.position.x - (Speed * Time.deltaTime), GameBoard.position.y, GameBoard.position.z);

					if (LastPieceOnBoard.position.x < 30f && Mathf.Abs(_lastSpawnedPosition - GameBoard.position.x) >= SpawnBlockIntervalDistance) {

							_lastSpawnedPosition = LastPieceOnBoard.position.x + UnityEngine.Random.Range (1f, SpawnBlockIntervalDistance);

							GameObject newBlock = Instantiate (Resources.Load (Prefab.name), GameBoard.position, Quaternion.identity) as GameObject;

							newBlock.transform.parent = GameBoard;

							float yPos = LastPieceOnBoard.position.y + UnityEngine.Random.Range (-3f, 3f);

							if (yPos < -31f)
									yPos = -31.1f;
							else if (yPos >= -17.6)
									yPos = -21.6f;

							newBlock.transform.position = new Vector3 (_lastSpawnedPosition, yPos, LastPieceOnBoard.position.z);

							newBlock.transform.localScale = new Vector3 (UnityEngine.Random.Range (20f, 40f), newBlock.transform.localScale.y, newBlock.transform.localScale.z);

							newBlock.transform.localRotation = new Quaternion (newBlock.transform.localRotation.x, newBlock.transform.localRotation.y, UnityEngine.Random.Range(0,360), newBlock.transform.localRotation.w);

							LastPieceOnBoard = newBlock.transform;
					}

					Speed += .0005f;
					SpawnBlockIntervalDistance += 0.005f;
				}
		}

		public void StartGame()
		{
				if (_gameIsRunning)
						return;

				_player = GameObject.Find ("Player").transform;
				_player.parent = transform;
				ScrollController.Object = _player;

				_gameIsRunning = true;
				_fadeTSObjects = true;

				ScrollController.Object = null;

				ScrollController.Button.onClick.RemoveAllListeners ();
				ScrollController.Button.onClick.AddListener (() => ButtonClick ());

				BubbleMessage.ShowMessage ("ss");

				Physics.gravity = new Vector3 (0, -12f, 0);
		}

		public void ButtonClick()
		{
				if (_jumpCount < 2) {
						_player.rigidbody2D.AddForce (Vector2.up * 170000f);
						AudioPlayer.Instance.Jump ();
				}

				_jumpCount++;
		}

		public void ResetJump()
		{
				_jumpCount = 0;
		}

		public void Fail()
		{
				_gameIsRunning = false;
				_positionFail = true;
				AudioPlayer.Instance.FailThenText ();
				_player.rigidbody2D.isKinematic = true;
				_player.rigidbody2D.gravityScale = 0f;
				Debug.Log ("Fail");
		}
		#endregion
}
