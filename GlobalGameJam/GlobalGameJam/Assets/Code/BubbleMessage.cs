﻿using UnityEngine;
using System.Collections;
using System;

public class BubbleMessage : MonoBehaviour {

		#region Private Static
		private static Transform _transform;
		private static SpriteRenderer _renderer;
		private static DateTime _showTime;
		private static bool _showMessage;
		#endregion

		#region Public Messages

		#endregion
	// Use this for initialization
	void Start () {
				_transform = transform;
				_renderer = transform.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
				if (_renderer.enabled && _showTime.AddMilliseconds (4000) < DateTime.Now) {
						_renderer.enabled = false;
				}
	}

		public static void ShowMessage(string message)
		{
				_renderer.sprite = Resources.Load<Sprite> (message);
				_renderer.enabled = true;
				_showTime = DateTime.Now;
		}
}
