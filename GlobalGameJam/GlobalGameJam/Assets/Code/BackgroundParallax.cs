﻿using System;
using UnityEngine;
using System.Collections.Generic;


public class BackgroundParallax : MonoBehaviour
{
		#region Public Properties
		public List<Transform> BackgroundLayers;
		public List<float> BackgroundLayerSpeeds;
		#endregion

		#region Private Variables
		private Vector2 savedOffset;
		#endregion

		#region Public Methods
		public void Update()
		{
				if (BackgroundLayers == null || BackgroundLayers.Count == 0)
						return;

				int idx = 0;
				foreach (Transform bg in BackgroundLayers) {
						float y = Mathf.Repeat (Time.time * BackgroundLayerSpeeds[idx], 1);
						Vector2 offset = new Vector2 (bg.renderer.sharedMaterial.GetTextureOffset ("_MainTex").x, y);
						bg.renderer.sharedMaterial.SetTextureOffset ("_MainTex", offset);
						idx++;
				}
		}
		#endregion
}

