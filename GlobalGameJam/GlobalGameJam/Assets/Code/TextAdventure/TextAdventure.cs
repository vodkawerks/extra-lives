﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class TextAdventure : MonoBehaviour {

		public Text FinalScore2;
		public void Choice1()
		{
				ScoreKeeper.Choice1 ();
		}
		public void Choice2()
		{
				ScoreKeeper.Choice2 ();
		}


		public void PizzaShop()
		{
				ScoreKeeper.PizzaShop ();
		}

		public void OneDollarTip()
		{
				ScoreKeeper.OneDollarTip ();
		}

		public void TwoDollarTip()
		{
				ScoreKeeper.TwoDollarTip ();
		}

		public void Reply()
		{
				ScoreKeeper.currentScore = 0;
				Application.LoadLevel (0);
		}

		public void Exit()
		{
				Application.Quit ();
		}

		public void FinalScore()
		{
				FinalScore2.text = "Score: " + ScoreKeeper.currentScore;
		}
}
