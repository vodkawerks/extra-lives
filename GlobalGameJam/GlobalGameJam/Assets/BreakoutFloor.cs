﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BreakoutFloor : MonoBehaviour {
	private BoxCollider2D floor;
	private BoxCollider2D leftWall;
	private BoxCollider2D rightWall;
	private BoxCollider2D topWall;

	[SerializeField]
	private PhysicsMaterial2D material;

	public static BreakoutFloor Instance;

	// Use this for initialization
	IEnumerator Start () {
		if (Instance == null) {
			Instance = this;
		} else {
			Destroy(this);
		}

		while (Camera.current == null) {
			yield return null;
		}
		Component[] colliders = gameObject.GetComponents<BoxCollider2D>();
		floor = colliders[0] as BoxCollider2D;
		leftWall = colliders[1] as BoxCollider2D;
		rightWall = colliders[2] as BoxCollider2D;
		topWall = colliders[3] as BoxCollider2D;
		
		//SetSizes();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void SetSizes () {
		Vector3[] wallPositions = {
			Camera.current.ViewportToWorldPoint(new Vector3(.5f, -.1f, 0)),
			Camera.current.ViewportToWorldPoint(new Vector3(1.1f, .5f, 0)),
			Camera.current.ViewportToWorldPoint(new Vector3(.5f, 1.1f, 0)),
			Camera.current.ViewportToWorldPoint(new Vector3(-.1f, .5f, 0))
		};
		float screenWidth = wallPositions[1].x - wallPositions[3].x;
		float screenHeight = wallPositions[2].y - wallPositions[0].y;

		for (int i=0; i<4; i++) {
			wallPositions[i] = new Vector3(wallPositions[i].x, wallPositions[i].y, 1);
		}

		floor.center = wallPositions[0];
		leftWall.center = wallPositions[3];
		rightWall.center = wallPositions[1];
		topWall.center = wallPositions[2];

		float colliderThickness = Camera.current.orthographicSize * 0.4f;  // magic value... I don't even know.
		float aspectRatio = Screen.width / (float)Screen.height;

		floor.size = new Vector3(screenWidth, colliderThickness, 1f);
		leftWall.size = new Vector3(colliderThickness * aspectRatio, screenHeight, 1f);
		rightWall.size = new Vector3(colliderThickness * aspectRatio, screenHeight, 1f);
		topWall.size = new Vector3(screenWidth, colliderThickness, 1f);

		floor.sharedMaterial = material;
		leftWall.sharedMaterial = material;
		rightWall.sharedMaterial = material;
		topWall.sharedMaterial = material;


	}

}
